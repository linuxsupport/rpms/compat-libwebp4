%global _hardened_build 1
%define _build_id_links none

Name:		compat-libwebp4
Version:	0.3.0
Release:	1%{?dist}
Group:		Development/Libraries
URL:		http://webmproject.org/
Summary:	Library and tools for the WebP graphics format
# Additional IPR is licensed as well. See PATENTS file for details
License:	BSD
Source0:	http://webp.googlecode.com/files/libwebp-%{version}.tar.gz
Patch0:		libwebp-0.3.0-endian-check.patch
Patch1:		libwebp-0.3.0-endian-check2.patch
BuildRequires:	libjpeg-devel libpng-devel libtool swig 
BuildRequires:  giflib4-devel
BuildRequires:  libtiff-devel
BuildRequires:	jpackage-utils

%description
Compatibility package with WebP libraries ABI version 4.

%prep
%setup -q -n libwebp-%{version}
%patch0 -p1 -b .endian
%patch1 -p1 -b .endian2

%build
mkdir -p m4
./autogen.sh
# enable libwebpmux since gif2webp depends on it
%configure --disable-static --enable-libwebpmux
make %{?_smp_mflags}

%install
%make_install
find "%{buildroot}/%{_libdir}" -type f -name "*.la" -delete

# Remove files that aren't needed for the compat package
rm -rf $RPM_BUILD_ROOT%{_bindir}
rm -rf $RPM_BUILD_ROOT%{_includedir}
rm -rf $RPM_BUILD_ROOT%{_libdir}/*.so
rm -rf $RPM_BUILD_ROOT%{_libdir}/icu/
rm -rf $RPM_BUILD_ROOT%{_libdir}/pkgconfig/
rm -rf $RPM_BUILD_ROOT%{_sbindir}
rm -rf $RPM_BUILD_ROOT%{_datadir}/icu/
rm -rf $RPM_BUILD_ROOT%{_mandir}

%post -n %{name} -p /sbin/ldconfig

%postun -n %{name} -p /sbin/ldconfig

%files -n %{name}
%{_libdir}/*.so.*

%changelog
* Wed Mar 11 2020 Ben Morrice <ben.morrice@cern.ch> - 0.3.0-1
- Initial release
